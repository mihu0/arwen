#include <QApplication>
#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QSerialPort>
#include <QSystemTrayIcon>
#include <QQueue>
#include <QMenu>
#include <QRadioButton>
#include <QSlider>
#include <QWidgetAction>
#include <QJsonDocument>
#include "qscale.h"

enum Mode {
  ModeUnknown = 0,
  ModeLux = 1,
  ModeLevel = 2,
};

struct CtrlStatus {

  CtrlStatus()
    : level(0), lux(0.0), mode(ModeUnknown)
  {}

  CtrlStatus(int _level, float _lux, Mode _mode)
    : level(_level), lux(_lux), mode(_mode)
  {}

  int level;
  float lux;
  Mode mode;
};

class Command : public QObject {
  Q_OBJECT
public:
  virtual QString commandLine() = 0;
  virtual void processResponseLine(const QByteArray &response) = 0;
};

class CommandSetLux : public Command {
  Q_OBJECT
public:
  CommandSetLux(float lux) : m_lux(lux) {}

  virtual QString commandLine() {
    return QString("X %1").arg(m_lux);
  }

  virtual void processResponseLine(const QByteArray &response) {
    float l = response.toFloat();
    emit configured(l);
  }

signals:
  void configured(float);

private:
  float m_lux;
};

class CommandSetLevel: public Command {
  Q_OBJECT
public:
  CommandSetLevel(int level) : m_level(level) {}

  virtual QString commandLine() {
    return QString("L %1").arg(m_level);
  }

  virtual void processResponseLine(const QByteArray &response) {
    int l = response.toInt();
    emit configured(l);
  }

signals:
  void configured(int);

private:
  int m_level;
};

class CommandStatus : public Command {
  Q_OBJECT
public:
  virtual QString commandLine() {
    return QString("S");
  }

  virtual void processResponseLine(const QByteArray &response) {
    auto parts = response.split(' ');

    if (parts.size() != 3) {
      return;
    }

    CtrlStatus status;

    if (parts[0][0] == 'X') {
      status.mode = ModeLux;
    } else if (parts[0][0] == 'L') {
      status.mode = ModeLevel;
    } else {
      status.mode = ModeUnknown;
    }

    status.lux = parts[1].toFloat();
    status.level = parts[2].toInt();

    emit statusChanged(status);
  }
signals:
  void statusChanged(CtrlStatus);
};

class CommandConfiguration : public Command {
  Q_OBJECT
public:
  virtual QString commandLine() {
    return QString("C");
  }

  virtual void processResponseLine(const QByteArray &response) {
    QJsonDocument doc = QJsonDocument::fromJson(response);
    emit configurationChanged(doc);
  }
signals:
  void configurationChanged(const QJsonDocument &);
};

class Protocol : public QObject {
  Q_OBJECT
public:
  Protocol()
    : m_serial("/dev/ttyUSB0"), m_currentCommand(nullptr)
  {
    m_serial.setBaudRate(QSerialPort::Baud115200);
    m_serial.open(QIODevice::ReadWrite);

    QTimer::singleShot(0, this, &Protocol::step);
  }

  void enqueue(Command *command) {
    m_commands.enqueue(command);
  }

private slots:
  void step() {
    bool activity = false;
    activity |= writeStep();
    activity |= readStep();
    QTimer::singleShot(activity ? 0 : 10, this, &Protocol::step);
  }

private:
  bool writeStep() {
    if (m_currentCommand) {
      return false;
    }

    if (m_commands.isEmpty()) {
      return false;
    }

    m_currentCommand = m_commands.dequeue();

    QString cmd = m_currentCommand->commandLine();
    //qDebug() << "Sending: " << cmd;
    m_serial.write((cmd + QLatin1String("\r")).toLatin1());
    m_serial.waitForBytesWritten();

    return true;
  }

  bool readStep() {
    int bytes_read = 0;

    while (m_serial.canReadLine()) {
      QByteArray response = m_serial.readLine();
      bytes_read += response.length();
      processLine(response);
    }

    return bytes_read > 0;
  }

  void processLine(const QByteArray &_line) {
    QByteArray line = _line.trimmed();

    if (line.isEmpty()) {
      return;
    }

    if (line[0] == 'D') {
      qDebug() << "Debug:" << line;
    } else if (line[0] == 'E') {
      qDebug() << "Error:" << line;
    } else if (line[0] == 'R') {
      //qDebug() << "Response:" << line;
      if (m_currentCommand) {
        QByteArray _line = line.right(line.length() - 2);
        m_currentCommand->processResponseLine(_line);
        delete m_currentCommand;
        m_currentCommand = nullptr;
      }
    } else {
      qDebug() << "Invalid:" << line;
    }
  }

private:
  QSerialPort m_serial;
  QQueue<Command *> m_commands;
  Command *m_currentCommand;
};

class Systray : public QObject {
  Q_OBJECT
public:
  Systray(Protocol &protocol) :
    QObject(),
    m_protocol(protocol)
  {
    QMenu *m = new QMenu();

    auto addAction = [&](QWidget *w) {
      QWidgetAction *wa = new QWidgetAction(nullptr);
      wa->setDefaultWidget(w);
      m->addAction(wa);
    };

    m->addSection("Flux");

    m_luxRadioButton = new QRadioButton();
    addAction(m_luxRadioButton);

    m_luxScale = new QScale();
    m_luxScale->setMaximum(100);
    m_luxScale->setOrientation(Qt::Horizontal);
    m_luxScale->setMinimumSize(200, 100);
    addAction(m_luxScale);

    m_luxSlider = new QSlider();
    m_luxSlider->setMaximum(100);
    m_luxSlider->setOrientation(Qt::Horizontal);
    m_luxSlider->setTickPosition(QSlider::TicksBothSides);
    addAction(m_luxSlider);

    m->addSection("Level");

    m_levelRadioButton = new QRadioButton();
    addAction(m_levelRadioButton);

    m_levelScale = new QScale();
    m_levelScale->setMaximum(255);
    m_levelScale->setOrientation(Qt::Horizontal);
    m_levelScale->setMinimumSize(200, 100);
    addAction(m_levelScale);

    m_levelSlider = new QSlider();
    m_levelSlider->setMaximum(255);
    m_levelSlider->setOrientation(Qt::Horizontal);
    m_levelSlider->setTickPosition(QSlider::TicksBothSides);
    m_levelSlider->setTickInterval(32);
    addAction(m_levelSlider);

    m->addSeparator();
    m->addAction(QIcon::fromTheme("application-exit"), "Quit", qApp, &QApplication::exit);

    QPixmap pm(32, 32);
    pm.load(":/icons/arwen.png");
    m_systray.setToolTip("Arwen Light Controller");
    m_systray.setIcon(pm);
    m_systray.setContextMenu(m);
    m_systray.setVisible(true);

    connect(m_luxSlider, &QSlider::sliderMoved, this, &Systray::setLux);
    connect(m_luxSlider, &QSlider::sliderPressed, this, &Systray::setLuxMode);
    connect(m_levelSlider, &QSlider::sliderMoved, this, &Systray::setLevel);
    connect(m_levelSlider, &QSlider::sliderPressed, this, &Systray::setLevelMode);
    connect(m_luxRadioButton, &QRadioButton::toggled, this, &Systray::setLuxMode);
    connect(m_levelRadioButton, &QRadioButton::toggled, this, &Systray::setLevelMode);
  }

public slots:
  void update(const CtrlStatus status) {
    m_sync = false;

    m_luxScale->setValue(status.lux);
    m_levelScale->setValue(status.level);

    if(status.mode == ModeLux) {
      m_levelSlider->setValue(status.level);
      m_luxRadioButton->setChecked(true);
    } else {
      m_luxSlider->setValue(status.lux);
      m_levelRadioButton->setChecked(true);
    }

    m_sync = true;
  }

  void setLuxMode() {
    if (m_sync)
      m_protocol.enqueue(new CommandSetLux(m_luxSlider->value()));
  }

  void setLux(int lux) {
    if (m_sync)
      m_protocol.enqueue(new CommandSetLux(lux));
  }

  void setLevelMode() {
    if (m_sync)
      m_protocol.enqueue(new CommandSetLevel(m_levelSlider->value()));
  }

  void setLevel(int lvl) {
    if (m_sync)
      m_protocol.enqueue(new CommandSetLevel(lvl));
  }

  void reconfigure(Mode mode, float lux, int level) {
    m_sync = false;

    if (mode == ModeLux) {
      m_luxSlider->setValue(lux);
      m_luxRadioButton->setChecked(true);
    } else {
      m_levelSlider->setValue(level);
      m_levelRadioButton->setChecked(true);
    }

    m_sync = true;
  }

private:
  QSystemTrayIcon m_systray;
  Protocol &m_protocol;

  QRadioButton *m_luxRadioButton;
  QRadioButton *m_levelRadioButton;
  QSlider *m_luxSlider;
  QSlider *m_levelSlider;
  QScale *m_luxScale;
  QScale *m_levelScale;
  bool m_sync;
};

class Arwen : public QObject {
  Q_OBJECT
public:
  Arwen() :
    QObject(),
    m_protocol(),
    m_systray(m_protocol)
  {
    QTimer::singleShot(100, this, &Arwen::configUpdate);
  }

private slots:
  void update() {
    auto c = new CommandStatus();
    connect(c, &CommandStatus::statusChanged, this, &Arwen::statusChanged);
    m_protocol.enqueue(c);
  }

  void statusChanged(const CtrlStatus status) {
    m_systray.update(status);
    QTimer::singleShot(100, this, &Arwen::update);
  }

  void configUpdate() {
    auto c = new CommandConfiguration();
    connect(c, &CommandConfiguration::configurationChanged, this, &Arwen::configurationChanged);
    m_protocol.enqueue(c);
  }

  void configurationChanged(const QJsonDocument &doc) {
    Mode mode;

    if (doc["Mode"] == "X") {
      mode = ModeLux;
    } else if (doc["Mode"] == "L") {
      mode = ModeLevel;
    }

    float lux = doc["TargetLux"].toDouble();
    int level = doc["TargetLevel"].toInt();

    m_systray.reconfigure(mode, lux, level);

    QTimer::singleShot(100, this, &Arwen::update);
  }

private:
  Protocol m_protocol;
  Systray m_systray;
};

#include "arwen.moc"

int main(int argc, char *argv[]) {
  QIcon::setThemeName("oxygen");
  QApplication app(argc, argv);
  Arwen a;
  return app.exec();
}
