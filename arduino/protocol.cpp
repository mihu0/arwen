#include <ArduinoJson.hpp>
#include <HardwareSerial.h>
#include "protocol.h"

#define PROTOCOL_DEBUG 0

Protocol::Protocol() : m_serial_port(Serial) {}

void Protocol::init() {
  m_serial_port.begin(115200);
  m_serial_port.setTimeout(1); /* NOTICE: this will directly influence how fast the loop() will go */
};

size_t Protocol::nextMessage(char **message) {
  size_t chars = m_serial_port.readBytesUntil('\n', &m_rx_buffer[m_last_read], BUFFER_SIZE - m_last_read - 1);

  if (chars == 0) {
    return 0;
  }

#if PROTOCOL_DEBUG
  m_serial_port.print("D c");
  m_serial_port.println(chars);
  m_serial_port.print("D ");
  m_serial_port.println(int(m_rx_buffer[m_last_read + chars - 1]));
#endif

  m_last_read += chars;

#if PROTOCOL_DEBUG
  m_serial_port.print("D LR");
  m_serial_port.println(m_last_read);
#endif

  /* buffer overrun, drop the content and start over */
  if (m_last_read == BUFFER_SIZE) {
    m_last_read = 0;
  }

  if (m_rx_buffer[m_last_read - 1] == '\n' || m_rx_buffer[m_last_read - 1]  == '\r') {
#if PROTOCOL_DEBUG
    m_serial_port.println("D eol");
#endif

    if (m_last_read == 1) {
      m_serial_port.println("E no cmd");
      return 0;
    }

    m_rx_buffer[m_last_read] = 0;
    m_last_read = 0;

    *message = m_rx_buffer;

#if PROTOCOL_DEBUG
  m_serial_port.println(String("D ") + message);
#endif

    return m_last_read - 1;
  }

  return 0;
}

void Protocol::sendStatus(Mode mode, float lux, short level) {
  char code;

  if (mode == ModeOff) {
    code = 'O';
  } else if (mode == ModeLux) {
    code = 'X';
  } else {
    code = 'L';
  }

  m_serial_port.print("R ");
  m_serial_port.print(code);
  m_serial_port.print(' ');
  m_serial_port.print(lux);
  m_serial_port.print(' ');
  m_serial_port.println(level);
}

void Protocol::sendResponse(int v) {
  m_serial_port.print("R ");
  m_serial_port.println(v);
}

void Protocol::sendResponse(float v) {
  m_serial_port.print("R ");
  m_serial_port.println(v);
}

void Protocol::sendError(char const *message) {
  m_serial_port.println("E ");
  m_serial_port.println(message);
}

void Protocol::sendConfiguration(const Configuration &c) {
  m_serial_port.print("R ");

  ArduinoJson::StaticJsonDocument<200> doc;
  if (c.getMode() == ModeLux) {
    doc["Mode"] = "X";
  } else {
    doc["Mode"] = "L";
  }
  doc["TargetLevel"] = c.getTargetLevel();
  doc["TargetLux"] = c.getTargetLux();
  ArduinoJson::serializeJson(doc, m_serial_port);
  m_serial_port.println("");
}
