#include "modes.h"

Lux::Lux(PWM &pwm) : m_pwm(pwm) {}

void Lux::init() {
  Wire.begin();
  m_bh1750.begin();
}

void Lux::step() {
  if (!m_bh1750.measurementReady())
    return;

  // TODO: sensor takes 100ms to take measurement, so we will adjust max 10/255 per second
  // we could possibly do better by actively changing sensor resolution
  float lux = m_bh1750.readLightLevel();

  adjust(lux);

  m_last_measured_lux = lux;
}

float Lux::targetLux() { return m_target_lux; }

float Lux::lux() { return m_last_measured_lux; }

float Lux::setLux(float v) {
  if (v == m_target_lux)
    return v;

  if (v < 0)
    m_target_lux = 0;
  else if (v > 1000)
    m_target_lux = 1000;
  else
    m_target_lux = v;

  return m_target_lux;
}

void Lux::resetFlip() {
  m_next_zero_change = m_next_flip_change = 0;
}

void Lux::adjust(float lux) {
  int step = (lux < m_target_lux) ? +1 : -1;
  int level = m_pwm.level();
  unsigned long now = millis();

  if ((level == 0 && step == 1) || (level == 1 && step == -1)) {
    // zero flip
    if (m_next_zero_change < now) {
      m_pwm.incrLevel(step);
      m_next_zero_change = now + MIN_INTERVAL_PER_ZERO_CHANGE;
    }
  } else if (abs(m_pwm.lastFlipLevel() - level) < 3) {
    // we are most likely flipping between two few level values
    if (m_next_flip_change < now) {
      m_pwm.incrLevel(step);
      m_next_flip_change = now + MIN_INTERVAL_PER_FLIP_CHANGE;
    }
  } else {
    m_pwm.incrLevel(step);
  }
}

Level::Level(PWM &pwm) : m_pwm(pwm) {}

void Level::step() {
  unsigned long now = millis();

  if (m_next_change < now) {
    advanceLevel();
    m_next_change = now + MIN_INTERVAL_PER_STEP;
  }
}

int Level::targetLevel() { return m_target_level; }
int Level::level() { return m_pwm.level(); }

int Level::setLevel(int v) {
  if (v < 0)
    m_target_level = 0;
  else if (v > 255)
    m_target_level = 255;
  else
    m_target_level = v;

  return m_target_level;
}

void Level::advanceLevel() {
  int level = m_pwm.level();

  if (level == m_target_level)
    return;

  m_pwm.incrLevel((level < m_target_level) ? 1 : -1);
}

Off::Off(PWM &pwm) : m_pwm(pwm) {}

void Off::step() {
  if (m_pwm.level() == 0)
    return;

  unsigned long now = millis();

  if (m_next_change < now) {
    m_pwm.incrLevel(-1);
    m_next_change = now + MIN_INTERVAL_PER_STEP;
  }
}

/* TODO: should we record all the configuration? */
void Off::activate(Mode mode) {
  m_restore_mode = mode;
  m_restore_level = m_pwm.level();
}

Mode Off::restoreMode() const { return m_restore_mode; }
short Off::restoreLevel() const { return m_restore_level; }
