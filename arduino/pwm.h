#ifndef PWM_h
#define PWM_h

#include <Arduino.h>

class PWM {
public:
  PWM(uint8_t pin);
  void init();

  short level() { return m_level; }
  void setLevel(int v);
  void incrLevel(short step);

  short lastStep() { return m_last_step; }
  short lastFlipLevel() { return  m_last_flip_level; }

private:
  uint8_t m_pin;
  short m_level;
  short m_last_step;
  short m_last_flip_level;
};

#endif
