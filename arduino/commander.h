#include "modes.h"
#include "protocol.h"
#include "configuration.h"

class Commander {
public:
  Commander();
  void init();
  void step();

private:
  void communicate();
  void protocol(char *message, size_t len);
  void watchdog();
  bool shouldTurnOff();

private:
  PWM m_pwm;
  Lux m_lux;
  Level m_level;
  Off m_off;
  Mode m_mode;
  Protocol m_protocol;
  Configuration m_configuration;

  unsigned long m_last_command_ts;
};
