#include "limits.h"
#include "commander.h"

const static long MAX_NO_COMMAND_WAIT_MILLIS = 10000;

Commander::Commander() : m_pwm(6), m_lux(m_pwm), m_level(m_pwm), m_off(m_pwm), m_mode(ModeLux) {
}

void Commander::init() {
  m_protocol.init();
  m_pwm.init();
  m_lux.init();

  m_mode = m_configuration.getMode();
  m_lux.setLux(m_configuration.getTargetLux());
  m_level.setLevel(m_configuration.getTargetLevel());
}

void Commander::step() {
  if (m_mode == ModeOff)
    m_off.step();
  else if (m_mode == ModeLevel)
    m_level.step();
  else {
    m_lux.step();
  }

  communicate();
  watchdog();
  m_configuration.step();
}

void Commander::communicate() {
  char *message;
  size_t len = m_protocol.nextMessage(&message);

  if (len) {
    protocol(message, len);
    m_last_command_ts = millis();
  }
}

void Commander::protocol(char *message, size_t len) {
  if (message[0] == 'S') {
    m_protocol.sendStatus(m_mode, m_lux.lux(), m_level.level());
  } else if (message[0] == 'X') {
    float n = atof(&message[2]);

    m_mode = ModeLux;
    n = m_lux.setLux(n);

    m_configuration.setMode(ModeLux);
    m_configuration.setTargetLux(n);

    m_protocol.sendResponse(n);
  } else if (message[0] == 'L') {
    int n = atol(&message[2]);

    m_mode = ModeLevel;
    n = m_level.setLevel(n);

    m_configuration.setMode(ModeLevel);
    m_configuration.setTargetLevel(n);

    m_protocol.sendResponse(n);
  } else if (message[0] == 'C') {
    m_protocol.sendConfiguration(m_configuration);
  } else {
    m_protocol.sendError("unknown cmd");
  }
}

void Commander::watchdog() {
  if (m_mode == ModeOff) {
    if (!shouldTurnOff()) {
      m_mode = m_off.restoreMode();

      if (m_mode == ModeLevel)
        m_level.setLevel(m_off.restoreLevel());
      else if (m_mode == ModeLux)
        m_lux.resetFlip();
    }
  } else {
    if (shouldTurnOff()) {
      m_off.activate(m_mode);
      m_mode = ModeOff;
    }
  }
}

bool Commander::shouldTurnOff() {
  unsigned long now = millis();

  long diff = 0;

  if (now < m_last_command_ts)
    /* clock recycled, this can still overflow after ~25days off, but who cares...  */
    diff = now + (ULONG_MAX - m_last_command_ts);
  else
    diff = now - m_last_command_ts;

  return diff > MAX_NO_COMMAND_WAIT_MILLIS;
}
