#ifndef MODES_H
#define MODES_H

#include <BH1750.h>
#include "pwm.h"
#include "globals.h"

class Lux {
  static const int MIN_INTERVAL_PER_ZERO_CHANGE = 60000;
  static const int MIN_INTERVAL_PER_FLIP_CHANGE = 1000;
public:
  Lux(PWM &pwm);

  void init();
  void step();
  float targetLux();
  float lux();

  float setLux(float v);
  void resetFlip();

private:
  void adjust(float lux);

private:
  PWM &m_pwm;
  BH1750 m_bh1750;

  float m_last_measured_lux;
  float m_target_lux;
  unsigned long m_next_zero_change;
  unsigned long m_next_flip_change;
};

class Level {
  static const int MIN_INTERVAL_PER_STEP = 10;
public:
  Level(PWM &pwm);

  void step();
  int targetLevel();
  int level();

  int setLevel(int v);

private:
  void advanceLevel();

private:
  PWM &m_pwm;
  int m_target_level;
  unsigned long m_next_change;
};

class Off {
  static const int MIN_INTERVAL_PER_STEP = 10;

public:
  Off(PWM &pwm);

  void step();
  void activate(Mode mode);

  Mode restoreMode() const;
  short restoreLevel() const;

private:
  PWM &m_pwm;
  Mode m_restore_mode;
  short m_restore_level;
  unsigned long m_next_change;
};

#endif
