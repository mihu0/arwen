#include "pwm.h"

PWM::PWM(uint8_t pin) : m_pin(pin) {}

void PWM::init() { setLevel(0); }

void PWM::setLevel(int v) {
  if (v < 0)
    m_level = 0;
  else if (v > 255)
    m_level = 255;
  else
    m_level = v;

  analogWrite(m_pin, m_level);
}

void PWM::incrLevel(short step) {
  if (m_last_step + step == 0)
    m_last_flip_level = m_level + step;

  m_last_step = step;

  return setLevel(m_level + step);
}
