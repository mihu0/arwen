#ifndef GLOBALS_H
#define GLOBALS_H

enum Mode : uint8_t {
  ModeOff = 0,
  ModeLux = 1,
  ModeLevel = 2
};

#endif
