#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "globals.h"

class EEPROMClass;
class Config;

class Configuration {
public:
  Configuration();
  ~Configuration();

  void step();

  float getTargetLux() const;
  void setTargetLux(float lux);

  int getTargetLevel() const;
  void setTargetLevel(int level);

  Mode getMode() const;
  void setMode(Mode mode);

  bool read();
  void write();

private:
  EEPROMClass &m_eeprom;
  Config *m_config;
  bool m_dirty;
  unsigned long m_next_write;
};

#endif
