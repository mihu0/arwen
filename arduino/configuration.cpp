#include <Arduino.h>
#include <EEPROM.h>
#include <string.h>
#include "configuration.h"

static const unsigned long GUARD = 822569775UL; /* just some random number */
static const uint8_t VERSION = 1;

static const unsigned long MINIMUM_SYNC_MILLIS = 30000;

struct Config {
  unsigned long guard;
  uint8_t version;
  Mode mode;
  int targetLevel;
  float targetLux;
};

Config *defaultConfig() {
  Config *c = new Config();
  c->guard = GUARD;
  c->version = 1;
  c->mode = ModeLux;
  c->targetLux = 20;
  return c;
}

Configuration::Configuration() : m_eeprom(EEPROM), m_config(defaultConfig()) {
  read();
}

Configuration::~Configuration() {
  delete m_config;
}

void Configuration::step() {
  if (!m_dirty)
    return;

  unsigned long now = millis();

  if (m_next_write > now)
    return;

  write();
  m_next_write = now + MINIMUM_SYNC_MILLIS;
}

float Configuration::getTargetLux() const {
  return m_config->targetLux;
}

void Configuration::setTargetLux(float lux) {
  m_config->targetLux = lux;
  m_dirty = true;
}

int Configuration::getTargetLevel() const {
  return m_config->targetLevel;
}

void Configuration::setTargetLevel(int level) {
  m_config->targetLevel = level;
  m_dirty = true;
}

Mode Configuration::getMode() const {
  return m_config->mode;
}

void Configuration::setMode(Mode mode) {
  m_config->mode = mode;
  m_dirty = true;
}

bool Configuration::read() {
  Config c;
  m_eeprom.get(0, c);

  if (c.guard == GUARD && c.version == VERSION) {
    memcpy(m_config, &c, sizeof(Config));
    m_dirty = false;
    return true;
  }

  return false;
}

void Configuration::write() {
  /* Library handles this slowly but gently towards EEPROM */
  m_eeprom.put(0, *m_config);
}
