#include "modes.h"
#include "configuration.h"

class HardwareSerial;

class Protocol {
  const static size_t BUFFER_SIZE = 64;

public:
  Protocol();

  void init();
  size_t nextMessage(char **message);

  void sendStatus(Mode mode, float lux, short level);
  void sendResponse(int);
  void sendResponse(float);
  void sendError(char const *message);
  void sendConfiguration(const Configuration &configuration);

private:
  size_t m_last_read;
  char m_rx_buffer[BUFFER_SIZE];
  HardwareSerial &m_serial_port;
};
