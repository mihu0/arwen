#include "commander.h"

Commander commander;

void setup() {
  commander.init();
}

void loop() {
  commander.step();
}
